

from . import ReadPulse
import threading
import time
import colorsys

import numpy as np
from pyglet.gl import *

vertices= (
    (1, 0, -1),
    (1, 1, -1),
    (-1, 1, -1),
    (-1, 0, -1),
    (1, 0, 1),
    (1, 1, 1),
    (-1, 0, 1),
    (-1, 1, 1)
    )

edges = (
    (0,1),
    (0,3),
    (0,4),
    (2,1),
    (2,3),
    (2,7),
    (6,3),
    (6,4),
    (6,7),
    (5,1),
    (5,4),
    (5,7)
    )


peak=0
rotate=0
MAX=15
bands=[0 for _ in range(MAX)]
bands2=[0 for _ in range(MAX)]
INCREMENT=5

class Window(pyglet.window.Window):
   xRotation = yRotation = 0
   ytranslate = xtranslate =0 
   ztranslate = -500
   color=0
   def __init__(self, width, height, title=''):
       super(Window, self).__init__(width, height, title,resizable=True)
       glClearColor(0, 0, 0, 1)
       glEnable(GL_DEPTH_TEST)
       pyglet.clock.schedule_interval(self.update_frame, 1/14)
      

   def on_draw(self):
       # Clear the current GL Window
       self.clear()

       # Push Matrix onto stack
       glPushMatrix()

       self.color+=1
       self.color%=256
       color=self.color
       glRotatef(self.xRotation, 1, 0, 0)
       glRotatef(self.yRotation, 0, 1, 0)
       # Draw the six sides of the cube
       for v,b in enumerate(bands):
        glBegin(GL_LINES)
        for edge in edges:
          m=b//4
          (re,gr,bl) = colorsys.hsv_to_rgb( ((v+color) % 255) / 256,1,1)
          glColor3f(re,gr,bl)
          for vertex in edge:
            glVertex3f( (-400+20*(15-v)) +vertices[vertex][0]*4, m*vertices[vertex][1]*2, vertices[vertex][2]*20 -40 )
        glEnd()

       for v,b in enumerate(bands2):
        glBegin(GL_LINES)
        for edge in edges:
          m=b//4
          (re,gr,bl) = colorsys.hsv_to_rgb( ((v+2*color) % 255) / 256,1,1)
          glColor3f(re,gr,bl)
          for vertex in edge:
            glVertex3f(20*v+vertices[vertex][0]*4, m*vertices[vertex][1]*2, vertices[vertex][2]*20 )
        glEnd()
       glPopMatrix()
       glLoadIdentity()
       glTranslatef(self.xtranslate, self.ytranslate, self.ztranslate)



   def on_mouse_drag(self,x,y,dx,dy,buttons,modifiers):
       if buttons & pyglet.window.mouse.LEFT:
           self.yRotation -= dx
           self.xRotation -= dy

   def on_mouse_scroll(self,x,y,scroll_x,scroll_y):
        self.ztranslate += INCREMENT * scroll_y *10
   

   def on_resize(self, width, height):
       # set the Viewport
       glShadeModel(GL_SMOOTH)
       glEnable(GL_COLOR_MATERIAL)

       glViewport(0, 0, width, height)
       # using Projection mode
       glMatrixMode(GL_PROJECTION)
       glLoadIdentity()

       aspectRatio = width / height
       gluPerspective(35, aspectRatio, 1, 2000)

       glMatrixMode(GL_MODELVIEW)
#       glLoadIdentity()
#       glTranslatef(0, self.ytranslate, -500)

   def on_text_motion(self, motion):
       if motion == pyglet.window.key.UP:
            self.xRotation += INCREMENT
       elif motion == pyglet.window.key.DOWN:
           self.xRotation -= INCREMENT
       elif motion == pyglet.window.key.LEFT:
           self.yRotation += INCREMENT
       elif motion == pyglet.window.key.RIGHT:
           self.yRotation -= INCREMENT

   def update(self,n):
      self.xRotation +=INCREMENT
      self.yRotation +=INCREMENT
#      print("update")
      return 0


   def update_frame(self,n):
    global pulse,bands,xRotation,yRotation
    dataL=pulse.dataL
    dataR=pulse.dataR
    if (len(dataL)>0):
       peak = np.abs(np.max(dataL)-np.min(dataL))/2**4
       bands.append(peak)
       bands.pop(0)
       peak = np.abs(np.max(dataR)-np.min(dataR))/2**4
       bands2.append(peak)
       bands2.pop(0)

    #print(bands)pyglet.app.run()
#    self.xRotation +=5
#    self.yRotation +=5


pulse= ReadPulse.ReadPulse()

def main():
   a    = threading.Thread(target=pulse.run,name="Pulse")
   a.start()

   Window(600, 300, 'PythonPulse Viz')
   #pyglet.clock.schedule_interval(update_frame, 1/24)
   pyglet.app.run()

   print("ok")
   pulse._quit()


if (__name__=="__main__"):
   main()
